php -r '
$custom = array( "showpassword"=>"yes", "bgcolor"=>"#ff0000");

function xor_encrypt($in) {
    $key = "qw8J";
    $text = $in;
    $outText = "";

    // Iterate through each character
    for($i=0;$i<strlen($text);$i++) {
        $outText .= $text[$i] ^ $key[$i % strlen($key)];
    }

    return $outText;
}

$tempdata = base64_encode(xor_encrypt(json_encode($custom)));
print($tempdata);
'
