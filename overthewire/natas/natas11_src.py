import os
import re
import requests

req = requests.post("http://natas11.natas.labs.overthewire.org/" +
                    "index-source.html",
                    auth=("natas11", PASSWORDS["natas11"]))
if not req:
    print(req.status_code)

html = "natas11_index_src.html"
path = "natas" + os.sep + html
print("Writing page soruce to '{}'".format(path))
src = open(path, "bw")
src.write(req.content)
src.close()
