import os
import re
import requests

req = requests.post("http://natas12.natas.labs.overthewire.org/"+
                    "index-source.html",
                    auth=("natas12", PASSWORDS["natas12"]))
if not req:
    print(req.status_code)

html = "natas12_index_src.html"
path = "natas" + os.sep + html
print("Writing page soruce to '{}'".format(path))
src = open(path, "bw")
src.write(req.content)
src.close()
