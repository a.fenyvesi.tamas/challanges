import os
import re
import requests

req = requests.post("http://natas9.natas.labs.overthewire.org/" +
                    "index-source.html",
                    auth=("natas9", PASSWORDS["natas9"]))
if not req:
    print(req.status_code)

html = "natas9_index_src.html"
path = "natas" + os.sep + html
print("Writing page soruce to '{}'".format(path))
src = open(path, "bw")
src.write(req.content)
src.close()
