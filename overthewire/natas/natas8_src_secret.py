## Extract the secret from the page source
path = "natas/natas8_index_src.html"
src_html = open(path)
data = src_html.readlines()
src_html.close()

# Clean data
data = [row.replace("&nbsp;", " ") for row in data]
data = "\n".join(data)
match = re.search("encodedSecret = \"(\S+)\"", data)
data = None

# Decode the encodedSecret
encoded = match.group(1)
print(encoded)
