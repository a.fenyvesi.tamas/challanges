secret="3d3d516343*********************"
import os
import re
import binascii
import base64
import requests

# Decode the encodedSecret
encoded = secret
encoded = binascii.unhexlify(encoded)
# Revers the string
encoded = encoded[::-1]
# Decode the base64
encoded = base64.b64decode(encoded)

params = {
    "submit": "1",
    "secret": encoded
}
req = requests.post("http://natas8.natas.labs.overthewire.org/",
                    auth=("natas8", PASSWORDS["natas8"]),
                    data=params)

if not req:
    print(req.text)
    print(req.status_code)
if "natas" in req.text:
    match = re.search("The password for natas\d is (\S{32})", req.text)
    print(match.group(1))
    PASSWORDS["natas9"] = match.group(1)
