import re
import requests

req = requests.get("http://natas0.natas.labs.overthewire.org/",
                   auth=("natas0", "natas0"))
if not req:
    print(req.status_code)
    sys.exit(1)
if "The password for nata" in req.text:
    match = re.search("The password for natas\d+ is (\S+)", req.text)
    PASSWORDS["natas1"] = match.group(1)
    print(match.group(1))
