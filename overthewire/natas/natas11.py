import re
import requests

cookies = {
    "data": "ClVLIh4ASC*********************FhRXJh4FGnBTVF4sQUcIelMK"
}
req = requests.post("http://natas11.natas.labs.overthewire.org/",
                    auth=("natas11", PASSWORDS["natas11"]),
                    cookies=cookies)

if not req:
    print(req.text)
    print(req.status_code)
if "natas" in req.text:
    match = re.search("is (\S{32})", req.text)
    print(match.group(1))
    PASSWORDS["natas12"] = match.group(1)
