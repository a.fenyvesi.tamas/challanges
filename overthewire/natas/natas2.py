import re
import requests

req = requests.get("http://natas1.natas.labs.overthewire.org/",
                   auth=("natas1", PASSWORDS["natas1"]))
if not req:
    print(req.status_code)
if "The password for nata" in req.text:
    match = re.search("The password for natas\d+ is (\S+)", req.text)
    PASSWORDS["natas2"] = match.group(1)
    print(match.group(1))
