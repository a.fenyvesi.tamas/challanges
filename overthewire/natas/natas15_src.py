import os
import re
import requests

req = requests.post("http://natas15.natas.labs.overthewire.org/"+
                    "index-source.html",
                    auth=("natas15", PASSWORDS["natas15"]))
if not req:
    print(req.status_code)

html = "natas15_index_src.html"
path = "natas" + os.sep + html
print("Writing page soruce to '{}'".format(path))
src = open(path, "bw")
src.write(req.content)
src.close()
