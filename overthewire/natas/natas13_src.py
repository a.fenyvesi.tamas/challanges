import os
import re
import requests

req = requests.post("http://natas13.natas.labs.overthewire.org/"+
                    "index-source.html",
                    auth=("natas13", PASSWORDS["natas13"]))
if not req:
    print(req.status_code)

html = "natas13_index_src.html"
path = "natas" + os.sep + html
print("Writing page soruce to '{}'".format(path))
src = open(path, "bw")
src.write(req.content)
src.close()
