php -r '
$defaultdata = array( "showpassword"=>"no", "bgcolor"=>"#ffffff");

function xor_encrypt($in) {
    $key = base64_decode("ClVLIh4ASC*********************EhhUJQNVAmhSEV4sFxFeaAw=");
    $text = $in;
    $outText = "";

    // Iterate through each character
    for($i=0; $i<strlen($text); $i++) {
        $outText .= $text[$i] ^ $key[$i % strlen($key)];
    }

    return $outText;
}
$tempdata = xor_encrypt(json_encode($defaultdata));

print($tempdata);
'
