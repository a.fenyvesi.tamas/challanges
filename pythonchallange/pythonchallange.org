* Pythonchallenge

Focused on image manipulation and thinking!! Unusual for my everday coding
habbits so i took the challenge.

You can start solving the leves from http://www.pythonchallenge.com/

*** Challenge 0
    We can see a picture on the web page. The next level is located in the same
    path. The page name is a number and is equal to 2^38
    Link: http://www.pythonchallenge.com/pc/def/0.html

    #+begin_src python :results output code :tangle challenge0.py
      import requests
      URL = "http://www.pythonchallenge.com/"

      number = pow(2, 38)
      print("2^38 is: "+str(number))
      resp = requests.get(URL+"/pc/def/"+str(number)+".html")
      print(resp.text)
    #+end_src

    #+RESULTS:
    #+begin_src python
    2^38 is: 274877906944
    <html>
    <head>
    <title>Redirection</title>
    <META HTTP-EQUIV="Refresh"
          CONTENT="0; URL=map.html">
    </head>
    <body>
    </body>
    </html>

    #+end_src

*** Challenge 1
    This webpage has a picture and some encoded text. It suggests a rot 13
    algorithm.

    Link: http://www.pythonchallenge.com/pc/def/map.html

    #+begin_src python :results output code :tangle challenge1.py
      import requests

      URL = "http://www.pythonchallenge.com/"
      CONST_STR = """g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr
      amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr
      ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq()
      gq pcamkkclbcb. lmu ynnjw ml rfc spj.""".replace("\n", "")


      def convert_str(msg):
          """
          Converts the provided string by moving each character 2 caraters right.
          In case we did not get an alphabetical char we startit from a again

          msg: the mesage you want to convert
          """
          print(msg)
          lower = ord('a')
          upper = ord('z')
          for a_char in msg.lower():
              if ord(a_char) not in range(lower, upper):
                  print(a_char, end="")
                  continue
              next_char = ord(a_char) + 2
              if next_char > upper:
                  next_char -= (upper - lower) + 1
              print(chr(next_char), end="")
          print()


      print("Original string:")
      convert_str(CONST_STR)

      print("URL was:")
      convert_str("http://www.pythonchallenge.com/pc/def/map.html")

      resp = requests.get("http://www.pythonchallenge.com/pc/def/ocr.jvon")
      print(resp.text)
    #+end_src

    #+RESULTS:
    #+begin_src python
    Original string:
    g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyramknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclrylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq()gq pcamkkclbcb. lmu ynnjw ml rfc spj.
    i hope you didnt translate it zy hand. thats whatcomputers are for. doing it in zy hand is inefficientand that's why this text is so long. using string.maketrans()is recommended. now apply on the url.
    URL was:
    http://www.pythonchallenge.com/pc/def/map.html
    jvvr://yyy.ravjqpejcnngpig.eqo/re/fgh/ocr.jvon
    Have you ever heard of jvon files !?

    #+end_src

*** Challenge 2
    Recognize the characters.
    Level 3 contains a large set of characters hidden in the DOM.
    We need to find the unique ones of them.

    Link: http://www.pythonchallenge.com/pc/def/ocr.html

    #+begin_src python :results output code :tangle challenge1.py
      import re
      import requests

      url = "http://www.pythonchallenge.com/pc/def/ocr.html"
      req = requests.get(url)
      raw_data = req.text.split("\n")[37:1257]
      counter_map = {}
      for row in raw_data:
          for element in row:
              if element not in counter_map.keys():
                  counter_map[element] = 1
              else:
                  counter_map[element] += 1
      solution = ''
      for key, value in counter_map.items():
          if value == 1:
              solution += key
      print("unique chars: " + solution)
    #+end_src

    #+RESULTS:
    #+begin_src python
    unique chars: equality
    #+end_src

*** Challenge 3
    One small letter, surrounded by <b>EXACTLY</b> three big bodyguards on each
    of its sides.

    Link: http://www.pythonchallenge.com/pc/def/equality.html

    #+begin_src python :results output code :tangle challenge3.py
      import re
      import requests

      req = requests.get("http://www.pythonchallenge.com/pc/def/equality.html")
      raw_data = "".join(req.text.split("\n")[37:1257])
      msg = ""
      for match in re.findall(r"[^A-Z]+[A-Z]{3}([a-z])[A-Z]{3}[^A-Z]+", raw_data):
          msg += match
      print(msg)
    #+end_src

    #+RESULTS:
    #+begin_src python
    linkedlist
    #+end_src

*** Challenge 4
    We have to play with the arguments here.

    Link: http://www.pythonchallenge.com/pc/def/linkedlist.php

    #+begin_src python :results output code :tangle challenge4.py
      import re
      import requests

      url = "http://www.pythonchallenge.com/pc/def/linkedlist.php"
      limit = 400
      number = 12345
      i = 0
      while i < limit:
          if i % 80 == 0:
              print("", flush=True)
          print(".", end="", flush=True)
          resp = requests.get(url, params={"nothing": number})
          match = re.search(r"the next nothing is (\d+)", resp.text)
          if match:
              number = match.group(1)
          elif "html" in resp.text:
              print("\n"+resp.text)
              break
          elif "Yes. Divide by two and keep going." in resp.text:
              number = round(int(number) / 2)
          else:
              print("\nAttempt nothing {} response:".format(number))
              print(resp.text)
              break
          i += 1
    #+end_src

    #+RESULTS:
    #+begin_src python

    ................................................................................
    ................................................................................
    ................................................................................
    ...........
    peak.html
    #+end_src
*** Challenge 5
    Pronounce it.

    On the picture we can see a hill. Saying out out loud many times peakhill
    you can associate to pickle. After unpickling the data we just need to print
    out it's content. It seems it is ascii art.

    Link: http://www.pythonchallenge.com/pc/def/peak.html

    #+begin_src python :results output code :tangle challenge5.py
      import requests

      url = "http://www.pythonchallenge.com/pc/def/banner.p"
      req = requests.get(url)

      import pickle
      data = pickle.loads(req.content)
      for row in data:
          for char,times in row:
              print(char* times, end="")
          print()
    #+end_src

    #+RESULTS:
    #+begin_src python

                  #####                                                                      ##### 
                   ####                                                                       #### 
                   ####                                                                       #### 
                   ####                                                                       #### 
                   ####                                                                       #### 
                   ####                                                                       #### 
                   ####                                                                       #### 
                   ####                                                                       #### 
          ###      ####   ###         ###       #####   ###    #####   ###          ###       #### 
       ###   ##    #### #######     ##  ###      #### #######   #### #######     ###  ###     #### 
      ###     ###  #####    ####   ###   ####    #####    ####  #####    ####   ###     ###   #### 
     ###           ####     ####   ###    ###    ####     ####  ####     ####  ###      ####  #### 
     ###           ####     ####          ###    ####     ####  ####     ####  ###       ###  #### 
    ####           ####     ####     ##   ###    ####     ####  ####     #### ####       ###  #### 
    ####           ####     ####   ##########    ####     ####  ####     #### ##############  #### 
    ####           ####     ####  ###    ####    ####     ####  ####     #### ####            #### 
    ####           ####     #### ####     ###    ####     ####  ####     #### ####            #### 
     ###           ####     #### ####     ###    ####     ####  ####     ####  ###            #### 
      ###      ##  ####     ####  ###    ####    ####     ####  ####     ####   ###      ##   #### 
       ###    ##   ####     ####   ###########   ####     ####  ####     ####    ###    ##    #### 
          ###     ######    #####    ##    #### ######    ###########    #####      ###      ######

    #+end_src
 
*** Challenge 6
    We can see a picture with a jeans on it. The focus is on the zip of the
    jeans. We have to deal with compressed file.

    #+begin_src python :results output code :tangle challenge6_pre.py
      import requests

      url = "http://www.pythonchallenge.com/pc/def/channel.zip"
      req = requests.get(url)
      zip = open("challenge6_channel_zip", "wb")
      zip.write(req.content)
      zip.close()

      print("zip file saved as 'challenge6_channel_zip'")
    #+end_src

    Now we have to investigate the compressed file. The readme.txt file give
    some useful hints. All we have to do is open the files in the correct order.
    The inforamtion required for the next level is in the zip information on the
    file. Once the message is built out of it we can see an ascii art again.

    #+begin_src python :results output code :tangle challenge6.py
      from zipfile import ZipFile
      import os

      if "level6_zipping" not in os.listdir("."):
          os.mkdir("level6_zipping")
      os.chdir("level6_zipping")

      zip_file = ZipFile("../challenge6_channel_zip")
      file_list = [zip_element.filename for zip_element in zip_file.filelist]
      help_file = "readme.txt"
      file_list.remove(help_file)
      zip_file.extract(member=help_file)
      init = open(help_file, "r").readlines()
      for line in init:
          print(line, end="")
          if "start from" in line:
              file_name = line.split(" ")[-1].rstrip() + ".txt"

      comments = []
      while file_name in file_list:
          zip_file.extract(member=file_name)
          data = open(file_name, "r").readlines()
          data = "".join(data)
          file_name = data.split(" ")[-1].rstrip() + ".txt"
          for zip_info in zip_file.filelist:
              if zip_info.filename != file_name:
                  continue
              comments.append(str(zip_info.comment, encoding="utf-8"))
              break
      zip_file.close()
      print("".join(comments))
    #+end_src

    #+RESULTS:
    #+begin_src python
    welcome to my zipped list.

    hint1: start from 90052
    hint2: answer is inside the zip
    ,***************************************************************
    ,****************************************************************
    ,**                                                            **
    ,**   OO    OO    XX      YYYY    GG    GG  EEEEEE NN      NN  **
    ,**   OO    OO  XXXXXX   YYYYYY   GG   GG   EEEEEE  NN    NN   **
    ,**   OO    OO XXX  XXX YYY   YY  GG GG     EE       NN  NN    **
    ,**   OOOOOOOO XX    XX YY        GGG       EEEEE     NNNN     **
    ,**   OOOOOOOO XX    XX YY        GGG       EEEEE      NN      **
    ,**   OO    OO XXX  XXX YYY   YY  GG GG     EE         NN      **
    ,**   OO    OO  XXXXXX   YYYYYY   GG   GG   EEEEEE     NN      **
    ,**   OO    OO    XX      YYYY    GG    GG  EEEEEE     NN      **
    ,**                                                            **
    ,****************************************************************
     ,**************************************************************

    #+end_src

    Caution! This ascii art can be misleading. To get to the next level you have
    to read the see oxygen in it :). Hockey is there just to mislead you

*** Challenge 7
    We can see a picture with a stripe on it.

    Link: http://www.pythonchallenge.com/pc/def/oxygen.html

    First save the image

    #+begin_src python :results output code :tangle challenge7_pre.py
      import requests

      url = "http://www.pythonchallenge.com/pc/def/oxygen.png"
      req = requests.get(url)

      image = open("challenge7_image.png", "wb")
      image.write(req.content)
      image.close()
      print("Saved image 'challenge7_image.png'")
    #+end_src

    #+RESULTS:
    #+begin_src python
    Saved image 'challenge7_image.png'
    #+end_src

    Start processing the image by getting the pixel data in the middle line.
    Since each channel holds the same value we only need one channel to continue
    working. If you look at the image closely the stripe repeates it color for 7
    pixels. It is even more visible when you convert the message to text. To
    remove the extra char we can use the python list slice builtin.

    #+begin_src python :results output code :tangle challenge7.py
      import re
      from PIL import Image

      img = Image.open("challenge7_image.png")
      pos_y = img.height / 2
      message = []
      print(str(img.getpixel((0, 0))))
      for pos_x in range(0, img.width):
          red, _, _, _ = img.getpixel((pos_x, pos_y))
          message.append(chr(red))
      img.close()
      del img

      clean_message = message[::7]
      print("".join(clean_message))
      concat = "".join(clean_message)
      secret = []
      for match in re.findall(r"(\d+)", concat):
          secret.append(match)
      print()
      print("interval: " + str(ord('a')) + "-" + str(ord('z')))
      for r in secret:
          print(chr(int(r)), end="")
    #+end_src

    #+RESULTS:
    #+begin_src python
    (79, 92, 23, 255)
    smart guy, you made it. the next level is [105, 110, 116, 101, 103, 114, 105, 116, 121]pe_

    interval: 97-122
    integrity
    #+end_src

*** Challange 8
    On this level we can see a picture again. This time a bee is on the picture.
    It does have an active link on the bee itself.

    #+begin_src python :results output code :tangle challenge8_pre.py
      import requests
      import re

      req = requests.get("http://pythonchallenge.com/pc/def/integrity.html")
      if not req:
          print(req.status_code)
      print(req.text)
    #+end_src

    #+RESULTS:
    #+begin_src python
    <html>
    <head>
      <title>working hard?</title>
      <link rel="stylesheet" type="text/css" href="../style.css">
    </head>
    <body>
      <br><br>
      <center>
      <img src="integrity.jpg" width="640" height="480" border="0" usemap="#notinsect"/>
      <map name="notinsect">
      <area shape="poly" 
        coords="179,284,214,311,255,320,281,226,319,224,363,309,339,222,371,225,411,229,404,242,415,252,428,233,428,214,394,207,383,205,390,195,423,192,439,193,442,209,440,215,450,221,457,226,469,202,475,187,494,188,494,169,498,147,491,121,477,136,481,96,471,94,458,98,444,91,420,87,405,92,391,88,376,82,350,79,330,82,314,85,305,90,299,96,290,103,276,110,262,114,225,123,212,125,185,133,138,144,118,160,97,168,87,176,110,180,145,176,153,176,150,182,137,190,126,194,121,198,126,203,151,205,160,195,168,217,169,234,170,260,174,282" 
        href="../return/good.html" />
      </map>
      <br><br>
      <font color="#303030" size="+2">Where is the missing link?</font>
    </body>
    </html>

    <!--
    un: 'BZh91AY&SYA\xaf\x82\r\x00\x00\x01\x01\x80\x02\xc0\x02\x00 \x00!\x9ah3M\x07<]\xc9\x14\xe1BA\x06\xbe\x084'
    pw: 'BZh91AY&SY\x94$|\x0e\x00\x00\x00\x81\x00\x03$ \x00!\x9ah3M\x13<]\xc9\x14\xe1BBP\x91\xf08'
    -->

    #+end_src

    We can see the link to the 'good.html' which probably leads to the next
    level. I tried to access the page but it is password protected. Luckily the
    webpage contains the required username and password combination. The only
    problem is that they are encrypted in some manner.

    After several approach it seems the un and password is a bzip2 compressed
    file content encoded in plain HTML.

    #+NAME: challenge8_creds
    #+begin_src python :results output code :tangle challenge8.py
      import re
      import requests

      req = requests.get("http://pythonchallenge.com/pc/def/integrity.html")
      if not req:
          print(req.status_code)

      credentials = []
      for line in req.text.split("\n"):
          if "un:" in line or "pw:" in line:
              line = re.search(r"\'(.*)\'", line).group()
              line = "".join(line)
              credentials.append(line)

      import bz2
      user = eval("b%s" % credentials[0])
      pwd = eval("b%s" % credentials[1])

      username = bz2.decompress(user).decode("utf-8")
      password = bz2.decompress(pwd).decode("utf-8")
      print("{}:{}".format(username, password))
    #+end_src

    #+RESULTS: challenge8_creds
    #+begin_src python
    huge:file
    #+end_src

    To make the upcomming levels easier i create a python session and include the passwords as token.

    #+begin_src python :var data=challenge8_creds :session pythonchallenge_cred :results output code
      from requests.auth import HTTPBasicAuth

      global TOKEN
      creds = data.rstrip().split(":")
      TOKEN = HTTPBasicAuth(creds[0], creds[1])
    #+end_src

*** Challenge 9
    Once we authenticated successfully into the level we can see a picture
    again. This time it has seemingly random dots on top of it.

    Link: http://www.pythonchallenge.com/pc/return/good.html

    #+begin_src python :session pythonchallenge_cred :results output code :tangle challenge9_pre.py
      import requests

      req = requests.get("http://www.pythonchallenge.com/pc/return/good.jpg", auth=TOKEN)
      image = open("challenge9_good.jpg", "wb")
      image.write(req.content)
      image.close()

      req = requests.get("http://www.pythonchallenge.com/pc/return/good.html", auth=TOKEN)
      global CHALLENGE_9_FIRST
      CHALLENGE_9_FIRST = []
      global CHALLENGE_9_SECOND
      CHALLENGE_9_SECOND = []

      first, second = False, False
      for line in req.text.split("\n"):
          if "first" in line:
              first = True
              continue
          elif first and line != "":
              line = line.split(",")
              if line[-1] == "":
                  line = line[0:-1]
              line = [int(string) for string in line ]
              CHALLENGE_9_FIRST += line
              continue
          else:
              first = False

          if "second" in line:
              second = True
              continue
          elif second and line != "":
              line = line.split(",")
              if line[-1] == "":
                  line = line[0:-1]
              line = [int(string) for string in line ]
              CHALLENGE_9_SECOND += line
              continue
          else:
              second = False
    #+end_src

    #+RESULTS:
    #+begin_src python
    #+end_src

    We saved a local copy of the image. The soultion is hidden on this image to
    manipulate it the local copy will be handy. Also exported the two number
    arrays into our python session. It will make our life much easier.

    #+begin_src python :session pythonchallenge_cred :results output code :tangle challenge9.py
      import requests
      from PIL import Image
      from PIL import ImageDraw

      first, second = CHALLENGE_9_FIRST, CHALLENGE_9_SECOND
      img = Image.open("challenge9_good.jpg")
      first = [int(s) for s in first]
      second = [int(s) for s in second]

      draw = ImageDraw.Draw(img)
      draw.polygon(first, fill=(0, 0, 0))
      draw.polygon(second, fill=(255, 255, 255))
      img.save("challenge9_good_modified.jpg")
    #+end_src

    After applied the layers on the image we can see a cow on the picture.

*** Challenge 10
    Another picture with two bulls on it. The question in this case is the
    length of an element in an array. The picture holds a link again to get a
    hint. It shows us the begging of the series.

    Link: http://www.pythonchallenge.com/pc/return/bull.html

    A bit of google and we know that this is about the look and say sequence.
    Generating the further elements should be straight forward.

    | idx | element | interpreded as                               |
    |-----+---------+----------------------------------------------|
    |   1 |       1 | 1 count of '1'                               |
    |   2 |      11 | 2 counts of '1'                              |
    |   3 |      21 | 1 count of '2' 1 count of '1'                |
    |   4 |    1211 | 1 count of '1' 1 count of '2' 2 count of '1' |
    |   5 |  111221 |                                              |

    #+begin_src python :session pythonchallenge_cred :results output code :tangle challenge10.py
      import sys

      def count_number(element):
          """
          Look-and-say-sequence
          """
          numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]

          new_element = []
          for number in numbers:
              if len(element) == 0 or number not in element:
                  continue
              count = 0
              while len(element) > 0 and element[0] == number:
                  count += 1
                  element.pop(0)
              if count == 0:
                  continue
              new_element.append(str(count))
              new_element.append(number)
          if len(element) > 0:
              new_element += count_number(element)
          return new_element

      element = ["1"]
      index = 0
      sys.setrecursionlimit(2000)
      while index < 30:
          element = count_number(element)
          index += 1
      print("Element length is {} at index {} "
            .format(len(element), index))

    #+end_src

    #+RESULTS:
    #+begin_src python
    Element length is 5808 at index 30
    #+end_src

*** Challenge 11
    Again we can see an image. This time is two image blended into one.

    Link: http://www.pythonchallenge.com/pc/return/5808.html

    #+begin_src python :session pythonchallenge_cred :results output code :tangle challenge11_pre.py
      import requests

      url = "http://www.pythonchallenge.com/pc/return/cave.jpg"
      req = requests.get(url, auth=TOKEN)
      image = open("challenge11_cave.jpg", "bw")
      image.write(req.content)
      image.close()
    #+end_src

    #+RESULTS:
    #+begin_src python
    #+end_src

    First we just saved the image to make it more available.

    #+begin_src python :session pythonchallenge_cred :results output code :tangle challenge11.py
    img = Image.open("challenge11_cave.jpg", "r")
    modified = Image.new(img.mode, (img.width, img.height))
    for row in range(0, img.width):
        if row % 2 != 0:
            continue
        for col in range(0, img.height):
            red, green, blue = img.getpixel((row, col))
            if col % 2 == 0:
                modified.putpixel((row, col), (red, green, blue))
    modified.save("challenge11_cave_modified.jpg")
    #+end_src

    #+RESULTS:
    #+begin_src python
    #+end_src

    I played around here quite a bit. I took every second row and column from
    the picture and then i get something useful. I can see some text on the
    image mentioning 'evil'.
